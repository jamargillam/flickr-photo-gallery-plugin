/*
	@Author: Jamar Gillam
	@Name: Flickr Gallery plugin
	@Version: 1.0 (Stable)
	@Description: This plugin is designed to create a mobile slideshow by accessing the Flickr Photo API. This plugin works only on the Cvent websites.
	@Dependencies: 
		#jQuery library is required. You can grab a copy of jQuery from the Google API website: https://developers.google.com/speed/libraries/#jquery.d
		#Swiper.js is required. Visit this website to download the library: http://www.idangero.us/swiper/#.VcDvapNVhBc.

*/
(function ($) {
 
	$.flickrgallery = function( options ) {
 
        //Default settings for the plugin
        var settings = $.extend({
            photoSetId: '',
            apiKey: '',
            showTitle: false,
            showDate: false,
            showUrl: false,
            showPhotoId: false,
            showAlbumTitle: false,
            showOwnerName: false,
            albumWrapper: '.swiper-wrapper',
            albumContainer: '.swiper-container'
            
        }, options );
        
        // Display default plugin options in the console
        console.log("Flickr Gallery Plugin Options" +
        			"\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" +
        			"\n\nDefault options with their respective default values" +
        			"\n********************************************************\n" + 
        			"\n@ photoSetId: ''," + 
        			"\n@ apiKey: ''," + 
        			"\n@ showTitle: false," +
        			"\n@ showDate: false," +
        			"\n@ showUrl: false," +
        			"\n@ showPhotoId: false," +
        			"\n@ showAlbumTitle: false," +
        			"\n@ showOwnerName: false," +
        			"\n@ albumWrapper: '.swiper-wrapper'," +
        			"\n@ albumContainer: '.swiper-container'"        			
		  );
		       
			try{	        	
		       
				/********************************************************************************
				*
				*	@Check for the photoset ID. You must pass in a photoset ID into the plugin.
				*
				*********************************************************************************/
				if( typeof settings.photoSetId !== "undefined" && settings.photoSetId !== "" ){
					var photoSetId = settings.photoSetId;			      
				}else{			      
					if( typeof settings.photoSetId === "undefined" ){
						console.error("Variable 'photoSetId' is undefined.");
					}else{
						console.error("Variable 'photoSetId' is blank. Please pass in a photoset ID.");
					}
				}
		       
				/********************************************************************************
				*
				*	@Check for the API key. You must pass in an API key into the plugin.
				*
				*********************************************************************************/
				if( typeof settings.apiKey !== "undefined" && settings.apiKey !== "" ){
					var apiKey = settings.apiKey;			      
				}else{			      
					if( typeof settings.apiKey === "undefined" ){
						console.error("Variable 'apiKey' is undefined.");
					}else{
						console.error("Variable 'apiKey' is blank. Please pass in an api key value.");
					}
				}
			   		       			   
				/**************************************************************************************
				*
				*	@buildGallery - Let's build our Flickr album here inside this function...
				*
				*************************************************************************************/
				this.buildGallery = function(){
					
					//Make the AJAX call to the Flickr REST API
					$.getJSON("https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&extras=url_m,date_upload&photoset_id=" + settings.photoSetId +"&api_key=" + settings.apiKey +"&format=json&jsoncallback=?", function(data){
				   						   						   
						var status = data.stat;
						var code = data.code;
						var errorMessage = data.message;
						
						/* Let's check for server errors */
						if ( status === 'fail'){
							$(settings.albumWrapper).prepend("<p class='errorMsg'>Error Code: " + code + "</p> <p class='errorMsg'>Status: " + status + "</p> <p class='errorMsg'>Message: " + errorMessage + "</p>");
						}else{
							//Loop through our results from the Flickr API
							$.each(data.photoset.photo, function( index, item ){
								var index = index + 1;
								var date = new Date(item.dateupload*1000); //Convert the UNIX timestamp into a human readable date
								var day = date.getDate(); //The day the photo was added to the Flickr album
								var year = date.getFullYear(); //Year the photo was added to the Flickr album
								var url = item.url_m; //Image url
								var title = item.title; //Image title
								var photoId = item.id; //Image id number
								var albumTitle = data.photoset.title; //Album title
								var ownerName = data.photoset.ownername; //Album owner name
								//Set up array to display the date		
								var month= new Array();
									month[0]="January";
									month[1]="February";
									month[2]="March";
									month[3]="April";
									month[4]="May";
									month[5]="June";
									month[6]="July";
									month[7]="August";
									month[8]="September";
									month[9]="October";
									month[10]="November";
									month[11]="December";
								var mon = month[date.getMonth()]; //Month
																			 
								//Append the contents of the API into DOM
								$(settings.albumWrapper).append("<div class='swiper-slide'> <img src='"+ url + "' class='feed-img' /> <br/> <p class='feed-title'>" + title + " </p> <p class='feed-date'>" + mon + " " + day+ ", " + year + "</p> <p class='url'><a href='"+ url +"'>" + url + "</a></p> <p class='photoId'>" + photoId + "</p> <p class='albumTitle'>" + albumTitle +"</p> <p class='ownerName'>" + ownerName +"</p></div>");	 													
							});//End photoset each loop
							
													
							   /***** #showTtitle option *****/       		
						       ( settings.showTitle ) ? $(".feed-title").show() : $(".feed-title").remove();
						       		              		
							   /***** #showDate option *****/        		
						       ( settings.showDate ) ? $(".feed-date").show() : $(".feed-date").remove();
						               		
							   /***** #showUrl option *****/
						       ( settings.showUrl ) ? $(".url").show() : $(".url").remove();
				
							   /***** #showPhotoId option *****/        		
						       ( settings.showPhotoId ) ? $(".photoId").show() : $(".photoId").remove();
				
							   /***** #showAlbumTitle option *****/        		
						       ( settings.showAlbumTitle ) ? $(".albumTitle").show() : $(".albumTitle").remove();
						       
						       /***** #showOwnerName option *****/
				  		       ( settings.showOwnerName ) ? $(".ownerName").show() : $(".ownerName").remove();
				  		       
				  		       //Set up the album using the Swiper.js plugin
				  		       var mySwiper = $(settings.albumContainer).swiper({ 
				  		       			mode:'horizontal', 
				  		       			loop: true 
				  		       	});
				  		       
				  			 }//End error status check		  			   
						  }).fail(function(jqXHR, textStatus, errorThrown){							  
							  alert("The album has encountered a server error. Please try again later.");							  
						});//End our AJAX call					       
					}//End buildGallery function
		    
		      //Call our gallery and inject into the DOM
		      this.buildGallery();
		      			 	       		       
	        }catch(error){ console.error("Error: " + error.message); } 
	};// #End the flickrgallery function
 
}( jQuery ));

$(window).on('load', function () {
    $(this).trigger('resize');
});
